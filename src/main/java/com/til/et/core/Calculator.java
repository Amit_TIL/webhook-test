/**
 *
 *
 */
package com.til.et.core;

/**
 * @author Amitkumar.Srivastava
 *
 * @created on Sep 27, 2019
 *
 */
public class Calculator {

	public int sum(int arg1, int arg2) {
		int sum = arg1 + arg2;
		System.out.println("Sum is : " + sum);
		return sum;
	}

}
