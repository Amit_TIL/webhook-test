import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.til.et.core.Calculator;

/**
 *
 *
 */

/**
 * @author Amitkumar.Srivastava
 *
 * @created on Sep 27, 2019
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {

	@InjectMocks
	Calculator calculator;

	/**
	 * Test method for {@link com.til.et.core.Calculator#sum(int, int)}.
	 */
	@Test
	public void testSum() {
		int sum = calculator.sum(3, 5);
		assertEquals("Sum should be 8.", 8, sum);
		//

	}

}
